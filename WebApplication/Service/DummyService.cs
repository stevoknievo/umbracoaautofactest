﻿using System.Collections.Generic;

namespace Indegrade.UmbSite.Service
{
    public class DummyService : IDummyService
    {
        public List<string> GetAll()
        {
            return new List<string> {"a", "b", "c"};
        }
    }

    public interface IDummyService
    {
        List<string> GetAll();
    }
}