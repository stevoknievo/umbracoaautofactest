﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.Compilation;
using System.Web.Hosting;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using Indegrade.UmbSite.Controllers;
using Umbraco.Core;
using Umbraco.Web.Trees;

namespace Indegrade.UmbSite.App_Code
{
    public class Startup : IApplicationEventHandler
    {
        public void OnApplicationInitialized(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
        }

        public void OnApplicationStarting(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
        }

        public void OnApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            var assemblies = GetApplicationAssemblies();
            BootAutofac(assemblies);

            //Get SectionService
            var sectionService = applicationContext.Services.SectionService;

            //Try & find a section with the alias of "mayhemSection"
            var mayhemSection = sectionService.GetSections().SingleOrDefault(x => x.Alias == "mayhem");

            //If we can't find the section - doesn't exist
            if (mayhemSection == null)
            {
                //So let's create it the section
                sectionService.MakeNew("Mayhem", "mayhem", "icon-pulse");
            }
        }

        private static void BootAutofac(Assembly[] assemblies)
        {
            var builder = new ContainerBuilder();

            builder.RegisterControllers(assemblies);

            builder.RegisterAssemblyTypes(assemblies)
                .Where(t => t.Name.EndsWith("Service"))
                .AsImplementedInterfaces();

            builder.RegisterApiControllers(typeof (ApplicationTreeController).Assembly);
            builder.RegisterApiControllers(typeof (RaceTreeController).Assembly);


            DependencyResolver.SetResolver(new AutofacDependencyResolver(builder.Build()));
        }

        private static Assembly[] GetApplicationAssemblies()
        {
            IEnumerable<Assembly> assemblies = AppDomain.CurrentDomain.GetAssemblies();
            if (HostingEnvironment.InClientBuildManager)
            {
                assemblies = assemblies.Union(BuildManager.GetReferencedAssemblies().Cast<Assembly>()).Distinct();
            }

            return assemblies.Where(assembly => assembly.GetName().Name.Contains("Indegrade")).ToArray();
        }
    }
}