﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http.Formatting;
using umbraco;
using umbraco.BusinessLogic.Actions;
using Umbraco.Core;
using Umbraco.Web.Models.Trees;
using Umbraco.Web.Mvc;
using Umbraco.Web.Trees;

namespace Indegrade.UmbSite.Controllers
{
    [PluginController("Mayhem")]
    [Tree("mayhem", "race", "Races", "icon-message", "icon-message", true, 5)]
    public class RaceTreeController : TreeController
    {
        private readonly IRaceService _raceService;

        public RaceTreeController(IRaceService raceService)
        {
            _raceService = raceService;
        }

        protected override TreeNodeCollection GetTreeNodes(string id, FormDataCollection queryStrings)
        {
            var nodes = new TreeNodeCollection();

            if (id == Constants.System.Root.ToString())
            {
                foreach (var series in _raceService.GetAllRaceSeries())
                {
                    var hasChildNodes = series.Series.Count > 0;
                    var nodeToAdd = CreateTreeNode(WebUtility.UrlEncode(series.Name), null, queryStrings, series.Name, "icon-tags", hasChildNodes);
                    nodeToAdd.RoutePath = "/mayhem/race/series/" + WebUtility.UrlEncode(series.Name);
                    nodes.Add(nodeToAdd);
                }
            }
            else
            {
                // get years from race series
                int s;
                if (!Int32.TryParse(id, out s))
                {
                    var series = _raceService.GetAllSeriesYears(id).Reverse();
                    foreach (var item in series)
                    {
                        var treeNode = CreateTreeNode(item.Year.ToString(), id, queryStrings, item.Id.ToString(), "icon-message", true);
                        treeNode.RoutePath = "/mayhem/race/year/" + item.Year;
                        nodes.Add(treeNode);
                    }
                }
                else // get all races
                {
                    var treeNode = CreateTreeNode("123", id, queryStrings, "05-06 B-Line", "icon-message", false);
                    nodes.Add(treeNode);
                }
            }

            return nodes;
        }

        protected override MenuItemCollection GetMenuForNode(string id, FormDataCollection queryStrings)
        {
            var menu = new MenuItemCollection();

            //If the node is the root node (top of tree)
            if (id == Constants.System.Root.ToInvariantString())
            {
                //Add in refresh action
                menu.Items.Add<RefreshNode, ActionRefresh>(ui.Text("actions", ActionRefresh.Instance.Alias), false);
            }

            return menu;
        }
    }
}