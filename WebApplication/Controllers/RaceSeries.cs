﻿using System.Collections.Generic;

namespace Indegrade.UmbSite.Controllers
{
    public class RaceSeries
    {
        public string Name { get; set; }
        public List<Series> Series { get; set; }
    }
}