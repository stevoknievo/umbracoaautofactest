﻿using System.Web.Mvc;
using Indegrade.UmbSite.Service;
using Umbraco.Web;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;

namespace Indegrade.UmbSite.Controllers
{
    public class HomeController : RenderMvcController
    {
        private readonly IDummyService _dummyService;

        public HomeController(IDummyService dummyService)
        {
            _dummyService = dummyService;
        }

        public override ActionResult Index(RenderModel model)
        {
            var t = _dummyService.GetAll();
            var w = CurrentPage.GetPropertyValue<string>("welcomeText");


            return CurrentTemplate(model);
        }
    }
}