﻿using System.Collections.Generic;

namespace Indegrade.UmbSite.Controllers
{
    public interface IRaceService
    {
        IList<RaceSeries> GetAllRaceSeries();
        IList<Series> GetAllSeriesYears(string id);
    }
}