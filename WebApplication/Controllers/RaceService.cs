﻿using System.Collections.Generic;

namespace Indegrade.UmbSite.Controllers
{
    public class RaceService : IRaceService
    {
        public IList<RaceSeries> GetAllRaceSeries()
        {
            var l = new List<RaceSeries>
            {
                new RaceSeries
                {
                    Name = "Race Series 1",
                    Series = new List<Series>
                    {
                        new Series {Id = 1, Name = "Race Series 1", Year = 2014},
                        new Series {Id = 2, Name = "Race Series 1", Year = 2015}
                    }
                },
                new RaceSeries
                {
                    Name = "Race Series 2",
                    Series = new List<Series>
                    {
                        new Series {Id = 3, Name = "Race Series 2", Year = 2014},
                        new Series {Id = 4, Name = "Race Series 2", Year = 2015}
                    }
                }
            };

            return l;
        }

        public IList<Series> GetAllSeriesYears(string id)
        {
            var l = new List<Series>();
            switch (id)
            {
                case "Race Series 1":
                    l = new List<Series>
                    {
                        new Series {Id = 1, Name = "Race Series 1", Year = 2014},
                        new Series {Id = 2, Name = "Race Series 1", Year = 2015}
                    };
                    break;
                case "Race Series 2":
                    l = new List<Series>
                    {
                        new Series {Id = 3, Name = "Race Series 2", Year = 2014},
                        new Series {Id = 4, Name = "Race Series 2", Year = 2015}
                    };
                    break;
            }

            return l;
        }
    }
}